﻿using Pazuzu156.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mshtml;
using System.Diagnostics;

namespace KSE
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private HTMLDocumentEvents2_Event docEvt;

		public MainWindow()
		{
			InitializeComponent();
			wbNewUpdate.Source = new Uri("http://cdn.kalebklein.com/kse/new/recent_changes.php?versionCode=1");
			wbNewUpdate.LoadCompleted += wbNewUpdate_LoadCompleted;
		}

		void wbNewUpdate_LoadCompleted(object sender, NavigationEventArgs e)
		{
			docEvt = (HTMLDocumentEvents2_Event)wbNewUpdate.Document;
			docEvt.oncontextmenu += docEvt_oncontextmenu;
		}

		bool docEvt_oncontextmenu(IHTMLEventObj pEvtObj)
		{
			wbCMenu.PlacementTarget = pEvtObj as ContextMenu;
			wbCMenu.IsOpen = true;
			return false;
		}

		private void bCancel_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void bUpdate_Click(object sender, RoutedEventArgs e)
		{

		}

		private void wbmiRefresh_Click(object sender, RoutedEventArgs e)
		{
			wbNewUpdate.Refresh();
		}

		private void wbmiViewURL_Click(object sender, RoutedEventArgs e)
		{
			new URLWindow(wbNewUpdate.Source.ToString()).ShowDialog();
		}

		private void wbmiBrowser_Click(object sender, RoutedEventArgs e)
		{
			Process.Start(wbNewUpdate.Source.ToString());
		}
	}
}
